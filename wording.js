define([
	'require',
	'madjoh_modules/language/language'
],
function(require, Language){
	var Wording = {
		numbers : ['number', 'year', 'day', 'hour'],

		init : function(settings){
			Wording.words 		= settings.words 	|| {};
			Wording.messages 	= settings.messages || {};
		},
		getText : function(key, values){
			if(!Wording.words[key]){
				console.log('Word not found !', key);
				var error = new Error();
				console.log(error.stack);
				return key;
			}

			var wordTrads = Wording.words[key];

			var isPlural = false;
			for(var param in values){
				if(Wording.numbers.indexOf(param) > -1){
					var number = (typeof values[param] === 'string') ? parseInt(values[param]) : values[param];
					isPlural = number !== 1;
					break;
				}
			}

			if(isPlural && wordTrads.plural) wordTrads = wordTrads.plural;

			var text = wordTrads[Language.get()];
			if(!text) text = wordTrads[Language.getDefault()];
			if(!values) return text;

			for(var param in values){
				reg = new RegExp('{{' + param + '}}', 'g');
				text = text.replace(reg, Wording.prettyBigNumbers(values[param], param));
			}

			return text;
		},

		prettyBigNumbers : function(number, key){
			if(isNaN(number) || key !== 'number') return number;

			var number    = parseInt(number);
			var precision = precision || 1;

			var units = ['', 'k', 'M', 'G'];

			var range = Math.floor(Math.log10(number)/3);

			var value = Math.floor(number/Math.pow(10, (range*3-precision)))/Math.pow(10, precision);
				value = (value >= 10) ? Math.floor(value) : value;

			if(value) 	return value + units[range];
			else 		return 0;
		}
	};

	return Wording;
});
